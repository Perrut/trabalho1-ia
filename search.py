# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def testeSearchProblem(problem):
    return depthSecondProblem(problem, problem.getStartState(), [])

def depthSecondProblem(problem, state, actions, visited = []):
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    n = Directions.NORTH
    e = Directions.EAST
    st = Directions.STOP

    if(problem.isGoalState(state)):
        return actions

    successors = problem.getSuccessors(state)
    for sucessor in successors:
        if sucessor not in visited:
            if sucessor[1] == 'North':
                actions.append(n)
                visited.append(sucessor)
                if problem.getCostOfActions(actions) == 999999:
                    del actions[-1]
                else:
                    actions = depthSecondProblem(problem, sucessor[0], actions, visited)
            elif sucessor[1] == 'South':
                actions.append(s)
                visited.append(sucessor)
                if problem.getCostOfActions(actions) == 999999:
                    del actions[-1]
                else:
                    actions =  depthSecondProblem(problem, sucessor[0], actions, visited)
            elif sucessor[1] == 'West':
                actions.append(w)
                visited.append(sucessor)
                if problem.getCostOfActions(actions) == 999999:
                    del actions[-1]
                else:
                    actions =  depthSecondProblem(problem, sucessor[0], actions, visited)
            elif sucessor[1] == 'East':
                actions.append(e)
                visited.append(sucessor)
                if problem.getCostOfActions(actions) == 999999:
                    del actions[-1]
                else:
                    actions =  depthSecondProblem(problem, sucessor[0], actions, visited)

    return actions

def depthFirstSearch(problem):
    visited = {}    # Cria dicionario que vai guardar quais vertices do grafo ja foram visitados
    state = problem.getStartState()    # Atribui a variavel "state" o estado inicial do problema ( no do grafo de onde a busca ira comecar)
    frontier = util.Stack()    # Cria uma pilha para salvar os vizinhos

    node = {}    # Cria um dicinario para ser a esrutura do nosso "no"
    node["parent"] = None   # Diz quem é o "no pai" desse "no"
    node["action"] = None    # Diz qual acao sera tomada quando atigir esse "no"

    node["state"] = state    # Diz qual o estado "no" atual
    frontier.push(node)    # Coloca o no inicial na pilha de vizinhos


    while not frontier.isEmpty():    # Enquanto a pilha de vizinhos não estiver vazia....
        node = frontier.pop()    # Pega o ultimo no
        state = node["state"]    # diz que o estado atual é o estado desse no

        if hash(state) in visited:    # verifica se esse no ja foi visitado
            continue    # caso tenha sido vizitado pula para a proxima iteracao
        visited[hash(state)] = True    # ja que nao foi visitado, vamos visita-lo

        if problem.isGoalState(state) == True:    # Verifica se esse estado é o obejtivo
            break    # Caso seja estado objetivo encerra o loop

        for child in problem.getSuccessors(state):    # Agora vamos expandir os filhos do estado atual

            if hash(child[0]) not in visited:   # Se o filho nao foi visitado...
                sub_node = {}    # cria a estrutura de "sub no", semlhante a estrutura no
                sub_node["parent"] = node    # diz qume é o pai desse "no"
                sub_node["state"] = child[0]    # diz quem é o no atual
                frontier.push(sub_node)    # coloca o subno na pilha de visinhos

    actions = []
    while node["action"] != None:     # nao entendi muito bem esse laco
        actions.insert(0, node["action"])
        node = node["parent"]

    return actions

def breadthFirstSearch(problem):
    frontier = util.Queue()    # unica difrenca pro dfs?
    visited = {}

    state = problem.getStartState()
    node = {}
    node["parent"] = None
    node["action"] = None
    node["state"] = state
    frontier.push(node)

    while not frontier.isEmpty():
        node = frontier.pop()
        state = node["state"]
        if hash(state) in visited:
            continue

        visited[hash(state)] = True
        if problem.isGoalState(state) == True:
            break

        for child in problem.getSuccessors(state):
            if child[0] not in visited:
                sub_node = {}
                sub_node["parent"] = node
                sub_node["state"] = child[0]
                sub_node["action"] = child[1]
                frontier.push(sub_node)

    actions = []
    while node["action"] != None:
        actions.insert(0, node["action"])
        node = node["parent"]

    return actions

def uniformCostSearch(problem):    # ignorar por enquanto
    frontier = util.PriorityQueue()
    start_state = problem.getStartState()
    frontier.push( (start_state, [], 0), 0 )
    visited = []

    while not frontier.isEmpty():
        state, actions, current_cost = frontier.pop()

        if(not state in visited):
            visited.append(state)

            if problem.isGoalState(state):
                return actions

            for child, direction, cost in problem.getSuccessors(state):
                frontier.push((child, actions+[direction], current_cost + cost), current_cost + cost)

    return []


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

class Noh(object):

    def __init__(self, a, b):
        self.custo = a
        self.estrutura = b

    def __lt__(self, a):
        return self.custo < a.custo

def aStarSearch(problem, heuristic=nullHeuristic):
    from game import Directions
    directionTable = {'South': Directions.SOUTH, 'North': Directions.NORTH,
                    'West': Directions.WEST, 'East': Directions.EAST}

    frontier = util.PriorityQueue()
    start_state = problem.getStartState()
    aux = Noh(0, (start_state, [], 0))
    frontier.push( aux, 0 )
    visited = []

    while not frontier.isEmpty():
        #state0, actions1, current_cost2 = frontier.pop()
        aux = frontier.pop()
        #aux = Noh(state, actions, current_cost)

        if problem.isGoalState(aux.estrutura[0]):
            return aux.estrutura[1]
        if aux.estrutura[0] not in visited:
            visited.append(aux.estrutura[0])
        for child, direction, cost in problem.getSuccessors(aux.estrutura[0]):
            if child not in visited:
                cost = problem.getCostOfActions(aux.estrutura[1] + [directionTable[direction]])
                frontier.push(Noh(aux.estrutura[2] + cost, (child, aux.estrutura[1] + [directionTable[direction]], aux.estrutura[2] + cost)), aux.estrutura[2] + heuristic(child, problem))
    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
